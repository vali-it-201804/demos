package day02;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Exercises {

	public static void main(String[] args) {

		// Muutuja, mis hoiaks väärtust 456.78
		float f1 = 456.78f;
		double d1 = 456.78d;
		BigDecimal bd1 = new BigDecimal("456.78");

		// Muutuja, mis viitaks tähtede jadale "test"
		String s1 = "test";

		// Defineeri muutuja, mis hoiaks tõeväärtust.
		boolean b1 = true;

		// Muutuja, mis viitab numbrite kogumile.
		// Variant 1:
		int[] numArray1 = null;
		numArray1 = new int[4];
		numArray1[0] = 5;
		numArray1[1] = 91;
		numArray1[2] = 304;
		numArray1[3] = 405;
		// System.out.println(numArray1[0]);
		// System.out.println(numArray1[1]);
		// System.out.println(numArray1[2]);
		// System.out.println(numArray1[3]);

		for (int i = 0; i < numArray1.length; i++) {
			System.out.println(numArray1[i]);
		}

		// Variant 2:
		int i1 = 6;
		int[] numArray2 = { 5, 91, 304, 405, i1 };

		// Muutuja, mis viitab komakohaga numbrite kogumile.
		float[] f2 = { 56.7f, 45.8f, 91.2f };

		// Muutujad, mis hoiavad väärtust 'a'.
		String s2 = "a";
		char c1 = 'a';
		byte b2 = (byte) 'a';
		System.out.println((char) b2);

		// Erineva iseloomuga muutujate kogum.
		String[] textArray = { "see on esimene väärtus", "67", "58.92" };

		// Suur number
		BigInteger bi1 = new BigInteger("7676868683452352345324534534523453245234523452345234523452345");
		System.out.println(bi1.multiply(new BigInteger("2")));
	}

}
