package day03;

import java.util.Scanner;

public class App {
	
	public final static double VAT_PERCENTAGE = 0.1d;
	
	public static void main(String[] args) {
				
		StringBuilder sb = new StringBuilder();
		String president1 = "Konstantin Päts";
		String president2 = "Lennart Meri";
		String president3 = "Arnold Rüütel";
		String president4 = "Toomas Hendrik Ilves";
		String president5 = "Kersti Kaljulaid";

		sb.append(president1)
		.append(", ")
		.append(president2)
		.append(", ")
		.append(president3)
		.append(", ")
		.append(president4)
		.append(" ja ")
		.append(president5)
		.append(" on Eesti presidendid.");
		System.out.println(sb.toString());
		
		String[] stringArray1 = {"text1", "text2", "text3"};
		
//		String resultStr1 = "";
		StringBuilder sb2 = new StringBuilder();
		for(String item : stringArray1) {
//			resultStr1 = resultStr1 + item;
			sb2.append(item);
		}
		System.out.println(sb2);
		
		printSentence("Rehepapp");
		printSentence("100 õhusõidukit");
		printSentence("Kokakunst");
		
//		String bookTitle = "Rehepapp";
//		String text1 = "Raamatu pealkiri on \"" + bookTitle + "\".";
//		System.out.println(text1);
		
		doExercise2();
		
		String someText = "test";
		String someText2 = new String("text");
		
		// Wrapper klassid ja tüübikonversioon
		int i = 7;
		Integer i2 = 7; // sama, mis new Integer(7);
		byte b = i2.byteValue();
		System.out.println(Integer.toBinaryString(7));
		int i3 = i + i2;
		
		System.out.println(args[0]);
		System.out.println(args[1]);
		
		int in1 = Integer.parseInt(args[0]);
		float in2 = Float.parseFloat(args[1]);
		float sum = in1 + in2;
		System.out.println(sum);
		
		String sumStr = ((Float)sum).toString();
		String sumStr2 = String.valueOf(sum);
		System.out.println(sumStr);
		
		// String split meetodi näide
		String str3 = "tekst1, tekts2, tekst3, tekst4, tekst5";
		String[] str3Arr = str3.split(", ");
		
		for(String strItem : str3Arr) {
			System.out.println(strItem);
		}
		
		double price = 100.0d;
		price = price + price * VAT_PERCENTAGE;
		System.out.println(price);
		
	}

	private static void printSentence(String bookTitle) {
		String text1 = "Raamatu pealkiri on \"" + bookTitle + "\".";
		System.out.println(text1);
	}
	
	private static void doExercise2() {
		String s1 = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
		Scanner sc = new Scanner(s1);
		sc.useDelimiter("Rida: ");
		while(sc.hasNext()) {
			System.out.println(sc.next());
		}
	}
	
}















