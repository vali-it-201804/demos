package day03;

public class ArrayDemo {

	public static void main(String[] args) {

		String[] user = { "Tanel", "Tuisk", "Elva" };
		
		String[][] users = { 
				{ "Marek", "Lints", "Tallinn", "Eesti" }, 
				{ "Leida", "Kuusk", "Tartu" },
				{ "Tanel", "Tuisk", "Elva" } ,
				user
			};
		
		String[][] users2 = new String[3][3];
		users2[0][0] = "Marek";
		users2[0][1] = "Lints";
		users2[0][2] = "Tallinn";
		users2[1][0] = "Leida";
		users2[1][1] = "Kuusk";
		users2[1][2] = "Tartu";
		users2[2] = new String[] {"Tanel", "Tuisk", "Elva"};
//		users[2][0] = "Tanel";
//		users[2][1] = "Tuisk";
//		users[2][2] = "Elva";
		
		for(String[] u : users2) {
			for (String userData : u) {
				System.out.print(userData + " ");
			}
			System.out.println();
		}

	}

}
