package day03;

public class SwitchDemo {

	public static void main(String[] args) {

		int doorStatus = 1;

		switch (doorStatus) {
		case 0:
			System.out.println("Door is closed");
			break;
		case 1:
		case 2:
			System.out.println("Door is little bit open.");
			System.out.println("Door is almost open.");
			break;
		case 3:
			System.out.println("Door is very open.");
			break;
		case 4:
			System.out.println("Door is gone!");
			break;
		default:
			System.out.println("Could not complete the command.");
		}

		String color = args[0].toUpperCase();

		switch (color) {
		case "GREEN":
			System.out.println("Driver can drive a car");
			break;
		case "YELLOW":
			System.out.println("Driver has to be raedy to stop the car");
			break;
		case "RED":
			System.out.println("Driver must stop the car");
			break;
		default:
			System.out.println("Have no idea what to do.");
		}

		if ("GREEN".equals(color)) {
			System.out.println("Driver can drive a car");
		} else if ("YELLOW".equals(color)) {
			System.out.println("Driver has to be raedy to stop the car");
		} else if ("RED".equals(color)) {
			System.out.println("Driver must stop the car");
		} else {
			System.out.println("Driver is confused");
		}

	}

}
