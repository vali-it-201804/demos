package day04;

import java.util.Random;

public class Exercises1 {

	public static void main(String[] args) {

		// Ülesanne 11
		int i = 1;
		while (i <= 100) {
			// System.out.println(i);
			i++;
		}

		// Ülesanne 12
		for (int x = 1; x <= 100; x++) {
			// System.out.println(x);
		}

		// Ülesanne 13
		int[] arr1 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int z : arr1) {
			// System.out.println(z);
		}

		// Ülesanne 14
		for (int x = 1; x <= 100; x++) {
			if (x % 3 == 0) {
				// System.out.println(x);
			}
		}

		// Ülesanne 15
		String[] arr2 = { "Sun", "Metsatöll", "Queen", "Metallica" };
		for (int x = 0; x < arr2.length; x++) {
//			System.out.print(arr2[x]);
			if (x < arr2.length - 1) {
//				System.out.print(", ");
			}
		}

		// Ülesanne 16
		for (int x = arr2.length - 1; x >= 0; x--) {
//			System.out.print(arr2[x]);
			if (x > 0) {
//				System.out.print(", ");
			}
		}
		
		// Ülesanne 17
//		for(String numString : args) {
//			int num = Integer.parseInt(numString);
//			switch(num) {
//			case 0:
//				System.out.println("null");
//				break;
//			case 1:
//				System.out.println("üks");
//				break;
//			case 2:
//				System.out.println("kaks");
//				break;
//			case 3:
//				System.out.println("kolm");
//				break;
//			case 4:
//				System.out.println("neli");
//				break;
//			case 5:
//				System.out.println("viis");
//				break;
//			case 6:
//				System.out.println("kuus");
//				break;
//			case 7:
//				System.out.println("seitse");
//				break;
//			case 8:
//				System.out.println("kaheksa");
//				break;
//			case 9:
//				System.out.println("üheksa");
//				break;
//			}
//		}
		
		// Ülesanne 18
		double randomNumber;
		do {
			System.out.println("tere");
			randomNumber = Math.random();
		} while(randomNumber < 0.5d);
		
	}

}




