package day04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Exercises2 {

	public static void main(String[] args) {

		// Ülesanne 19
		List<String> cities = new ArrayList<>();
		cities.add("Tallinn");
		cities.add("Pärnu");
		cities.add("Nürnberg");
		cities.add("Fürth");
		cities.add("Helme");
		System.out.println(String.format("Esimene: %s, teine: %s, kolmas: %s", cities.get(0), cities.get(2),
				cities.get(cities.size() - 1)));

		// Ülesanne 20
		LinkedList<String> queue1 = new LinkedList<>();
		queue1.add("üks");
		queue1.add("kaks");
		queue1.add("kolm");
		queue1.add("neli");
		queue1.add("viis");
		queue1.add("kuus");
		queue1.push("seitse");
		queue1.push("kaheksa");
		// queue1.forEach(x -> { x = x.toUpperCase(); });
		queue1.replaceAll(x -> {
			return x.substring(0, 1).toUpperCase() + x.substring(1, x.length());
		});
		System.out.println(queue1);
		while (!queue1.isEmpty()) {
			// System.out.println(queue1.remove());
			System.out.println(queue1.pop());
		}
		System.out.println(queue1);

		// Ülesanne 21
		Set<String> set1 = new TreeSet<>();
		set1.addAll(Arrays.asList("Tauri", "Tanel", "Lauri", "Laura", "Mihkel"));
		set1.forEach(x -> System.out.println(x));

		// Ülesanne 22
		Map<String, String[]> countryCities = new HashMap<>();
		countryCities.put("Estonia", new String[] { "Tallinn", "Tartu", "Valga", "Võru" });
		countryCities.put("Sweden", new String[] { "Stockholm", "Uppsala", "Lund", "Köping" });
		countryCities.put("Finland", new String[] { "Helsinki", "Espoo", "Hanko", "Jämsä" });

		// Printout: Variant 1
		for (String key : countryCities.keySet()) {
			System.out.println("Country: " + key);
			System.out.println("Cities:");
			for (String city : countryCities.get(key)) {
				System.out.println("    " + city);
			}
		}

		// Printout: Variant 2
		countryCities.forEach((countryName, cityNames) -> {
			System.out.println("Country: " + countryName + "\nCities:" );
			for(String cityName : cityNames) {
				System.out.println("    " + cityName);
			}
		});

	}

}
