package day04;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Exercises3 {

	public static void main(String[] args) {
		
		// Klassikalin for
		for (long i = 0; i < 10_000L; i = i + 1) {
			
		}
		
		// For each
		for(int i : new int[] {5, 6, 8, 9}) {
			System.out.println(i);
		}
		
		// While
		boolean shouldContinue = true;
		while (shouldContinue) {
			if (Math.random() > 0.2d) {
				System.out.println("Continue looping.");
			} else {
				System.out.println("Finish!");
				shouldContinue = false;
			}
		}
		
		// Do-while
		shouldContinue = true;
		do {
			System.out.println("Jätkan ketramist...");
			if (Math.random() > 0.9) {
				System.out.println("Lõpetan tsükli töö.");
//				shouldContinue = false;
				break;
			}
		} while(shouldContinue);
		
		// Kollektsioonid
		
		// nimekiri elementidest
		List<String> list1 = new ArrayList<>();
		
		// unikaalne nimekiri elementidest
		Set<String> set1 = new HashSet<>();
		
		// võti --> väärtus paaride nimekiri (dictionary)
		Map<String, String> map1 = new HashMap<>();
		
		// Kollektsioon vs massiiv (array):
		// Massiivil kindel pikkus, kollektsiooni pikkus võib muutuda.
		// Kollektsioonil palju abimeetodeid, massiivil need puuduvad.
		// Massiiv ei oska enda sisu välja printida, kollektsioon oskab.
		// Massiiv raiskab vähem ressursse, kollektsioon on kallis (mälu, protsessori jõudlus).
		
		
	}

}
