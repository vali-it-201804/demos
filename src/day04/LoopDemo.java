package day04;

public class LoopDemo {

	public static void main(String[] args) {

		int[] arr1 = { 19, 29, 39, 49, 59 };

		// FOR (klassikaline)

		for (int i = 0; i < arr1.length; i++) {
			System.out.println("TERE " + arr1[i]);
		}

		for (int i = arr1.length - 1; i >= 0; i--) { // i = 4, 3, 2, 1, 0, -1 (finish)
			System.out.println("TERE " + arr1[i]);
		}

		// Igavesed tsüklid
		// for (;;);
		// while(true);

		// WHILE

		int u = 0;
		while (u < arr1.length) {
			System.out.println("TERE " + arr1[u++]);
		}

		int j = arr1.length - 1;
		while (j >= 0) {
			System.out.println("TERE " + arr1[j--]);
		}

		// FOREACH

		for (int x : arr1) {
			System.out.println("TERE " + x);
		}

		// DO-WHILE

		// Peab olema kindel, et massiivil on vähemalt 1 element!
		int k = 0;
		do {
			System.out.println("TERE " + arr1[k++]);
		} while (k < arr1.length);

		// BREAK && CONTINUE
		
		for(int i = 1; i < 10000; i++) {
			if (i < 650) {
				System.out.println("Lõpetame selle tsükli tegevuse.");
				continue; // jätkame uue tsükliga
			}
			if (i % 650 == 0) {
				System.out.println(i);
				break; // lõpetame tsükli tegevuse
			}
		}
		
		// Kole ja ebakorrektne lahendus!
		someLabel:
		for (int i = 0; i < 10; i++) {
			for (int s = 456; s < 634; s++) {
				if (s == 500) {
					break someLabel;
				}
			}
		}
		
		// Viisakas lahendus
		someFuncWithTwoLoops();
	}
	
	private static void someFuncWithTwoLoops() {
		for (int i = 0; i < 10; i++) {
			for (int s = 456; s < 634; s++) {
				if (s == 500) {
					return;
				}
			}
		}
	}

	
	
}






