package day04;

import java.util.Random;
import java.util.Scanner;

public class NerdGame {

	public static void main(String[] args) {
		System.out.println("Paku number vahemikus 1 - 10 000.");
		Scanner inputScanner = new Scanner(System.in);
		String decision; // Deklareerin muutuja
		do {
			play(inputScanner); // ALT + SHIFT + R - muudab meetodi/muutuja nime läbi kogu koodi
			System.out.println("Kas soovid mängida uuesti? y/n");
			decision = inputScanner.next(); // Väärtustan muutuja
		} while(decision.equals("y"));
		
		System.out.println("Copyright: Marek Lints. All rights reserved!");
	}

	// Meetod = funktsioon = subrutiin = protseduur
	private static void play(Scanner inputScanner) {
		int randomNumber = new Random().nextInt(10000) + 1;
		int counter = 1;
		int userGuess = 0;
		do {
			System.out.println("Sinu pakkumine: ");
			userGuess = inputScanner.nextInt();
			if (userGuess == randomNumber) {
				System.out.println(String.format("Õige, sul kulus %s korda, et ära arvata.", counter));
			} else if (userGuess < randomNumber) {
				System.out.println("Liiga väike.");
				counter++;
			} else {
				System.out.println("Liiga Suur.");
				counter++;
			}
		} while (userGuess != randomNumber); // Tingimus on tõene siis, kui kasutaja arvas valesti.!= on mittevõrdumise operaator
	}
	
}
