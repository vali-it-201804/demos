package day05;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Exercises {

	public static void main(String[] args) {

		// Ülesanne 1:
		// System.out.println("######");
		// System.out.println("#####");
		// System.out.println("####");
		// System.out.println("###");
		// System.out.println("##");
		// System.out.println("#");

		// etapp 1:
		// for (int i = 0; i < 6; i++) {
		// System.out.println("######");
		// }

		// etapp 2:
		// for (int i = 0; i < 6; i++) {
		// System.out.print("#");
		// }

		// etapp 3:
		for (int i = 0; i < 6; i++) {
			int lineLength = 6 - i;
			for (int j = 0; j < lineLength; j++) {
				System.out.print("#");
			}
			System.out.println();
		}

		// Ülesanne 2:
		for (int i = 0; i < 6; i++) {
			int lineLength = i + 1;
			for (int j = 0; j < lineLength; j++) {
				System.out.print("#");
			}
			System.out.println();
		}
		
		
		// Ülesanne 3:
		for (int i = 0; i < 5; i++) {

			// Nähtamatu kolmnurk
			for (int j = 0; j < 5 - i; j++) {
				System.out.print(" ");
			}

			// Nähtav kolmnurk
			for (int k = 0; k < i + 1; k++) {
				System.out.print("@");
			}

			System.out.println();
		}

		// Ülesanne 4:
		String number = "123456789";
		System.out.println("NUMBER: " + number);
		char[] digits = number.toCharArray();
		String reversedNumber = "";
		for (int i = 0; i < digits.length; i++) {
			reversedNumber = digits[i] + reversedNumber;
		}
		System.out.println("REVERSED NUMBER: " + reversedNumber);

		// Ülesanne 5:
		int examResult = Integer.parseInt(args[0]);
		String status;
		String mark = null;
		if (examResult < 51) {
			status = "FAIL";
		} else if (examResult >= 51 && examResult <= 60) {
			status = "PASS";
			mark = "1";
		} else if (examResult >= 61 && examResult <= 70) {
			status = "PASS";
			mark = "2";
		} else if (examResult >= 71 && examResult <= 80) {
			status = "PASS";
			mark = "3";
		} else if (examResult >= 81 && examResult <= 90) {
			status = "PASS";
			mark = "4";
		} else {
			status = "PASS";
			mark = "5";
		}

		if (status.equals("FAIL")) {
			System.out.println("FAIL");
		} else {
			System.out.println(String.format("%s - %s, %s", status, mark, examResult));
		}

		// Ülesanne 6:
		double[][] triangleSides = { { 34.6d, 56.8d }, { 45.7d, 45.0d }, { 78.0f, 67.3d }, { 65.3d, 76.4d } };
		for (double[] sides : triangleSides) {
			double result = Math.sqrt(Math.pow(sides[0], 2) + Math.pow(sides[1], 2));
			System.out.println(result);
		}

		// Ülesanne 7A:
		String[][] countries = { { "Estonia", "Tallinn", "Jüri Ratas" }, { "Latvia", "Riga", "Māris Kučinskis" },
				{ "Germany", "Berlin", "Angela Merkel" }, { "United Kingdom", "London", "Theresa May" } };

		for (String[] countryInfo : countries) {
			System.out.println(countryInfo[2]);
		}

		for (String[] countryInfo : countries) {
			System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s", countryInfo[0],
					countryInfo[1], countryInfo[2]));
		}

		// Ülesanne 7B
		Object[][] countries2 = new Object[][] {
				{ "Estonia", "Tallinn", "Jüri Ratas",
						new String[] { "Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish" } },
				{ "Latvia", "Riga", "Māris Kučinskis",
						new String[] { "Latvian", "Russian", "Ukrainian", "Belarusian", "Finnish" } },
				{ "Germany", "Berlin", "Angela Merkel", new String[] { "German", "Polish", "Spanish" } },
				{ "United Kingdom", "London", "Theresa May", new String[] { "English", "French", "Russian" } } };

		for (int i = 0; i < countries2.length; i++) {
			Object[] countryInfo = countries2[i];
			System.out.println(countryInfo[0] + ":");
			String[] languages = (String[]) countryInfo[3];
			for (String language : languages) {
				System.out.println("        " + language);
			}
		}

		Object[][] oa = new Object[][] { { "xxx1", "yyy1", new String[] { "aaa1", "bbb1", "ccc1" } },
				{ "xxx2", "yyy2", new String[] { "aaa2", "bbb2", "ccc2" } }, };

		for (int i = 0; i < oa.length; i++) {
			Object[] oaElement = oa[i];
			System.out.println(oaElement[0] + " " + oaElement[1]);
			String[] languges = (String[]) oa[i][2];
			for (String language : languges) {
				System.out.println("    " + language);
			}
		}

		// Ülesanne 8
		List<Object> countries3 = new ArrayList<>();
		countries3.add(Arrays.asList("Estonia", "Tallinn", "Jüri Ratas",
				Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish")));
		countries3.add(Arrays.asList("Latvia", "Riga", "Māris Kučinskis",
				Arrays.asList("Latvian", "Russian", "Ukrainian", "Belarusian", "Finnish")));
		countries3
				.add(Arrays.asList("Germany", "Berlin", "Angela Merkel", Arrays.asList("German", "Polish", "Spanish")));
		countries3.add(Arrays.asList("United Kingdom", "London", "Theresa May",
				Arrays.asList("English", "French", "Russian")));

		System.out.println("++++++++++++++++++");
		for (Object countryInfo : countries3) {
			List<Object> countryInfoDetails = (List<Object>) countryInfo;
			List<String> languages = (List<String>) countryInfoDetails.get(3);
			System.out.println(countryInfoDetails.get(0));
			for (String language : languages) {
				System.out.println("         " + language);
			}
		}

		// Ülesanne 9
		System.out.println("++++++++++++++++++");
		Map<String, List<String>> countries4 = new HashMap<>();
		countries4.put("Estonia", Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"));
		countries4.put("Latvia", Arrays.asList("Latvian", "Russian", "Ukrainian", "Belarusian", "Finnish"));
		countries4.put("Germany", Arrays.asList("German", "Polish", "Spanish"));
		countries4.put("United Kingdom", Arrays.asList("English", "French", "Russian"));

		for (String country : countries4.keySet()) {
			System.out.println(country);
			for (String language : countries4.get(country)) {
				System.out.println("        " + language);
			}
		}

		// Ülesanne 10
		System.out.println("+++++++++++++++++++");
		LinkedList<Object> countries5 = new LinkedList<>();
		countries5.add(Arrays.asList("Estonia", "Tallinn", "Jüri Ratas",
				Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish")));
		countries5.add(Arrays.asList("Latvia", "Riga", "Māris Kučinskis",
				Arrays.asList("Latvian", "Russian", "Ukrainian", "Belarusian", "Finnish")));
		countries5
				.add(Arrays.asList("Germany", "Berlin", "Angela Merkel", Arrays.asList("German", "Polish", "Spanish")));
		countries5.add(Arrays.asList("United Kingdom", "London", "Theresa May",
				Arrays.asList("English", "French", "Russian")));
		
		while(!countries5.isEmpty()) {
			List<Object> countryInfo = (List<Object>)countries5.remove();
			List<String> languages = (List<String>)countryInfo.get(3);
			System.out.println("Country name: " + countryInfo.get(0));
			for(String language : languages) {
				System.out.println("        " + language);
			}
		}
		
		// Lisa: Ülesanne 3:
		int height = 5;
		int width = 7;
		for (int u = 0; u < height; u++) {
			for (int s = 0; s < width; s++) {				
				if (u % 2 == 0) {
					if (s % 3 == 0) {
						System.out.print("#");
					} else {
						System.out.print("+");
					}
				} else {
					System.out.print("=");
				}
			}
			System.out.println();
		}
	}
}

