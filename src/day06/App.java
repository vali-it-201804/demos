package day06;

public class App {

	public static void main(String[] args) {
		int i = 5;
		double pi = 3.1419d;
		double pi2 = getPi();
		int numberPow2 = doSomething(i);
	}
	
	public static double getPi() {
		return 3.14159d;
	}
	
	public static int doSomething(int someNumber) {
		return someNumber * someNumber;
	}

}
