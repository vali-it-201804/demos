package day06;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AthletExampleExercise {

	public static void main(String[] args) {
		Runner myRunner1 = new Runner();
		myRunner1.setFirstName("Aita-Leida");
		myRunner1.setLastName("Kuusepuu");
		myRunner1.setAge(45);
		myRunner1.setGender("F");
		myRunner1.setWeight(64.4d);

		Runner myRunner2 = new Runner();
		myRunner2.setFirstName("Peeter");
		myRunner2.setLastName("Purustaja");
		myRunner2.setAge(34);
		myRunner2.setGender("M");
		myRunner2.setWeight(75.4d);

		Runner myRunner3 = new Runner();
		myRunner3.setFirstName("Lauri");
		myRunner3.setLastName("Lahe");
		myRunner3.setAge(27);
		myRunner3.setGender("M");
		myRunner3.setWeight(89.4d);
		
		Skydiver mySkydiver1 = new Skydiver();
		mySkydiver1.setFirstName("Raul");
		mySkydiver1.setLastName("Rebane");
		mySkydiver1.setAge(67);
		mySkydiver1.setGender("M");
		mySkydiver1.setWeight(89.4d);
		
		Skydiver mySkydiver2 = new Skydiver();
		mySkydiver2.setFirstName("Leo");
		mySkydiver2.setLastName("Lendur");
		mySkydiver2.setAge(35);
		mySkydiver2.setGender("M");
		mySkydiver2.setWeight(57.8d);
		
		Runner myRunner4 = new Runner();
		myRunner4.setFirstName("Jüri");
		myRunner4.setLastName("Kuusepuu");
		myRunner4.setAge(43);
		myRunner4.setGender("M");
		myRunner4.setWeight(98.4d);
		
		Runner myRunner5 = new Runner();
		myRunner5.setFirstName("Leeni");
		myRunner5.setLastName("Rebane");
		myRunner5.setAge(32);
		myRunner5.setGender("F");
		myRunner5.setWeight(56.1d);
		
		Skydiver mySkydiver3 = new Skydiver();
		mySkydiver3.setFirstName("Leo");
		mySkydiver3.setLastName("Lendur");
		mySkydiver3.setAge(35);
		mySkydiver3.setGender("M");
		mySkydiver3.setWeight(77.8d);

		List<Athlete> athletes = new ArrayList<>();
		athletes.add(myRunner1);
		athletes.add(myRunner2);
		athletes.add(myRunner3);
		athletes.add(mySkydiver1);
		athletes.add(mySkydiver2);
		athletes.add(myRunner4);
		athletes.add(myRunner5);
		athletes.add(mySkydiver3);
		System.out.println(athletes);
		
		athletes.sort(new AthleteComparator());
		
//		athletes.sort(
//			new Comparator<Athlete>() {
//
//				@Override
//				public int compare(Athlete o1, Athlete o2) {
//					// Sorting by last name.
//					int result = o1.getLastName().compareTo(o2.getLastName());		
//					
//					if (result == 0) {
//						// Sorting by first name.
//						result = o1.getFirstName().compareTo(o2.getFirstName());
//						
//						if (result == 0) {
//							// Sorting by age.
//							result = o1.getAge() - o2.getAge();
//							
//							// Sorting by weight.
//							if (result == 0) {
//								double weightDiff = o1.getWeight() - o2.getWeight();
//								if (weightDiff > 0.0) {
//									result = 1;
//								} else if (weightDiff < 0.0) {
//									result = -1;
//								} else {
//									result = 0;
//								}
//								
//								// Sorting by gender.
//								result = o1.getGender().compareTo(o2.getGender());
//							}
//						}
//					}
//							
//					return result;
//				}
//		}
//		
//	);
		
//		athletes.sort((o1, o2) -> {
//			// Sorting by last name.
//			int result = o1.getLastName().compareTo(o2.getLastName());		
//			
//			if (result == 0) {
//				// Sorting by first name.
//				result = o1.getFirstName().compareTo(o2.getFirstName());
//				
//				if (result == 0) {
//					// Sorting by age.
//					result = o1.getAge() - o2.getAge();
//					
//					// Sorting by weight.
//					if (result == 0) {
//						double weightDiff = o1.getWeight() - o2.getWeight();
//						if (weightDiff > 0.0) {
//							result = 1;
//						} else if (weightDiff < 0.0) {
//							result = -1;
//						} else {
//							result = 0;
//						}
//						
//						// Sorting by gender.
//						result = o1.getGender().compareTo(o2.getGender());
//					}
//				}
//			}
//					
//			return result;
//		});
		
		System.out.println(athletes);
				
//		Skydiver mySkydiver = new Skydiver();
//		Athlete myGenericAthlete = new Athlete();
		
//		List<Athlete> athletes = new ArrayList<>();
//		athletes.add(myRunner);
//		athletes.add(mySkydiver);
////		athletes.add(myGenericAthlete);
//		
//		for(Athlete athlete : athletes) {
//			athlete.perform();
//		}
		
	}

}
