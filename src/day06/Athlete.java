package day06;

public abstract class Athlete {

	protected String firstName;
	protected String lastName;
	protected int age;
	protected String gender;
	protected int length;
	protected double weight;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getLength() {
		return length;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public abstract void perform();

	protected void prepareForPerform() {
		// Valmistame sportlase ette sportimiseks.
	}

	// {
	// System.out.println("Default implementation. Please implement me in
	// subclass.");
	// }

	@Override
	public String toString() {
		return String.format("\n[fn=%s, ln=%s, a=%s, g=%s, w=%s]", this.getFirstName(), this.getLastName(),
				this.getAge(), this.getGender(), this.getWeight());
	}

}








