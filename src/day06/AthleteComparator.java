package day06;

import java.util.Comparator;

public class AthleteComparator implements Comparator<Athlete> {

	@Override
	public int compare(Athlete o1, Athlete o2) {
		
		// Sorting by last name.
		int result = o1.getLastName().compareTo(o2.getLastName());		
		
		if (result == 0) {
			// Sorting by first name.
			result = o1.getFirstName().compareTo(o2.getFirstName());
			
			if (result == 0) {
				// Sorting by age.
				result = o1.getAge() - o2.getAge();
				
				// Sorting by weight.
				if (result == 0) {
					double weightDiff = o1.getWeight() - o2.getWeight();
					if (weightDiff > 0.0) {
						result = 1;
					} else if (weightDiff < 0.0) {
						result = -1;
					} else {
						result = 0;
					}
					
					// Sorting by gender.
					result = o1.getGender().compareTo(o2.getGender());
				}
			}
		}
				
		return result;
		
//		if (o1.getAge() < o2.getAge()) {
//			return -1;
//		} else if (o1.getAge() > o2.getAge()) {
//			return 1;
//		}
//		
//		return 0;
		
		// Kui o1 on väiksem, kui o2 --> tagasta negatiivne number
		// Kui o1 on suurem, kui o2 --> tagasta positiivne number
		// kui o1 ja o2 on võrdsed --> tagasta 0
	}

}
