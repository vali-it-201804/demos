package day06;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CountryInfo {

	public String name;
	public String capital;
	public String primeMinister;
	public List<String> languages = new ArrayList<>();

//	public CountryInfo() {
//		
//	}
	
	public CountryInfo(String name, String capital, String primeMinister, Collection<String> languages) {
		this.name = name;
		this.capital = capital;
		this.primeMinister = primeMinister;
		this.languages.addAll(languages);
	}

	@Override
	public String toString() {
		String textOutput = this.name;
		textOutput += ":\n";
		for (String language : this.languages) {
			textOutput += "        " + language + "\n";
		}
		return textOutput;
	}

}
