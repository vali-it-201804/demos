package day06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Excercises2 {

	public static void main(String[] args) {

		CountryInfo estonia = new CountryInfo("Estonia", "Tallinn", "Jüri Ratas",
				Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"));

		CountryInfo latvia = new CountryInfo("Latvia", "Riga", "Māris Kučinskis", 
				Arrays.asList("Latvian", "Russian", "Belarusian", "Ukrainian", "Polish"));
		
		List<CountryInfo> countries = new ArrayList<>();
		countries.add(estonia);
		countries.add(latvia);
		
		for(CountryInfo country : countries) {
			System.out.println(country);
		}
		
//		CountryInfo poland = new CountryInfo();
		
	}

}
