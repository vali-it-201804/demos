package day06;

import java.math.BigInteger;

public class Exercises1 {

	public static void main(String[] args) {

		boolean isTrue = test(3);

		if (test(35)) {
			System.out.println("OK");
		}

		test2("text1", "text2");
		test(56);

		double priceWithVat = addVat(100.0d);
		System.out.println(priceWithVat);
		if (addVat(100.0) == 120) {

		}
		int[] xxxArr = xxx(3, 4, true);
		for (int n : xxxArr) {
			System.out.println(n);
		}
		
		System.out.println(deriveGender("38104"));
		System.out.println(deriveGender("611"));

		boolean springEst = isItSpring("Estonia", 4);
		boolean springOtherWorld = isItSpring("Rest of the world", 4);

		System.out.println("Is it spring in Estonia? " + isItSpring("Estonia", 4));
		System.out.println("Is it spring? " + isItSpring("Rest of the world", 4));

		System.out.println(deriveBirthYear("38104242729"));
		System.out.println(deriveBirthYear("16204242729"));
		System.out.println(deriveBirthYear("56204242729"));
		System.out.println(deriveBirthYear(null));
		
		System.out.println(isCheckNumberCorrect(new BigInteger("38104242729")));
		
//		for (int i = 100; i > 0; i--) {
//			System.out.println(i);
//		}
		
		printOutNumbers(100);

	}
	
	static void printOutNumbers(long i) {
		System.out.println(i);
		i = i - 1;
		if (i > 0) {
			printOutNumbers(i);
		}
	}

	static boolean test(int x) {
		if (x > 5) {
			return true;
		} else {
			return false;
		}
	}

	static void test2(String p1, String p2) {

	}

	static double addVat(double input) {
		double priceWithVat = 1.2d * input;
		return priceWithVat;
	}

	static int[] xxx(int n1, int n2, boolean t) {
		if (t) {
			return new int[] { n1 * n1, n2 * n2 };
		}
		return null; // seda saab teha ainult siis, kui tagastustüüp on objekt.
	}

	public static String deriveGender(String personalCode) { // personalCode = 3......
		if (personalCode != null) {
			String genderNum = personalCode.substring(0, 1);
			int gender = Integer.parseInt(genderNum);
			if (gender % 2 == 0) {
				return "F";
			} else {
				return "M";
			}
		}
		return null;
	}
	
	private static String deriveGender(String s2, int i3) {
		return null;
	}

	public static boolean isItSpring(String country, int month) {
		if ("Estonia".equals(country)) {
			return false;
		} else {
			return month > 2 && month < 6;
		}
	}

	public static int deriveBirthYear(String personalCode) {
		if (personalCode != null && personalCode.length() == 11) {
			int centuryDeterminant = Integer.parseInt(personalCode.substring(0, 1));
			int birthYearInCentury = Integer.parseInt(personalCode.substring(1, 3));

			if (centuryDeterminant == 1 || centuryDeterminant == 2) {
				return 1800 + birthYearInCentury;
			} else if (centuryDeterminant == 3 || centuryDeterminant == 4) {
				return 1900 + birthYearInCentury;
			} else if (centuryDeterminant == 5 || centuryDeterminant == 6) {
				return 2000 + birthYearInCentury;
			}

			// switch (centuryDeterminant) {
			// case 1:
			// case 2:
			// return 1800 + birthYearInCentury;
			// case 3:
			// case 4:
			// return 1900 + birthYearInCentury;
			// case 5:
			// case 6:
			// return 2000 + birthYearInCentury;
			// }
		}
		return 0;
	}

	public static boolean isCheckNumberCorrect(BigInteger personalCode) {
		if (personalCode != null) {
			String personalCodeStr = personalCode.toString();
			if (personalCodeStr.length() == 11) {
				// Case 1
				int[] weights1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1 };
				int tmpSum = 0;
				for (int i = 0; i < weights1.length; i++) {
					String personalCodeDigitStr = personalCodeStr.substring(i, i + 1);
					int digit = Integer.parseInt(personalCodeDigitStr);
					tmpSum = tmpSum + digit * weights1[i];
				}
				int checkNumber = tmpSum % 11;
				String personalCodeLastDigit = personalCodeStr.substring(10, 11);
				if (checkNumber != 10) {
					int personalCodeCheckNumber = Integer.parseInt(personalCodeLastDigit);
					return personalCodeCheckNumber == checkNumber;
				} else {
					// Case 2
					int[] weights2 = { 3, 4, 5, 6, 7, 8, 9, 1, 2, 3 };
					tmpSum = 0;
					for (int i = 0; i < weights2.length; i++) {
						String personalCodeDigitStr = personalCodeStr.substring(i, i + 1);
						int digit = Integer.parseInt(personalCodeDigitStr);
						tmpSum += digit * weights2[i];
					}
					checkNumber = tmpSum % 11;
					checkNumber = (checkNumber == 10) ? 0 : checkNumber;
					int personalCodeCheckNumber = Integer.parseInt(personalCodeLastDigit);
					return personalCodeCheckNumber == checkNumber;
				}

			}
		}
		return false;
	}

	public static boolean isPersonalCodeCorrect(String personalCode) {
		boolean result = isCheckNumberCorrect(new BigInteger(personalCode));
		if (result) {
			int month = Integer.parseInt(personalCode.substring(3, 5));
			result = month > 0 && month < 13;
		}
		
		int month = Integer.parseInt(personalCode.substring(3, 5));
		int day = Integer.parseInt(personalCode.substring(5, 7));
		if (result) {
			if (month == 2) {
				result = day > 0 && day <= 29;
			} else {
				switch (month) {
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					result = day > 0 && day < 32;
					break;
				default:
					result = day > 0 && day < 31;
				}
			}
		}
		
		if (result) {
			int firstDigit = Integer.parseInt(personalCode.substring(0, 1));
			result = firstDigit > 0 && firstDigit < 7;
		}
		return result;
	}

}
