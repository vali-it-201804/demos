package day06;

public class FileLogger {

	private static FileLogger fileLogger = null;
	
	private FileLogger() {
		
	}
	
	public static FileLogger getFileLoggerSingeltonInstance() {
		if (FileLogger.fileLogger == null) {
			FileLogger.fileLogger = new FileLogger();
		}
		return FileLogger.fileLogger;
	}
	
	public void doSomeSingeltonAction(String message) {
		// Loogika, mida tohib teha vaid üksainus objekt.
	}
	
}
