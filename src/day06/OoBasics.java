package day06;

import java.util.Scanner;

public class OoBasics {

	public static void main(String[] args) {
		
//		OoBasics ooBasics = new OoBasics();
		
		Dog myCoolDog = new Dog();
		myCoolDog.setName("Muki");
		myCoolDog.setAge(7);
		myCoolDog.setTailLength(40);
		System.out.println(myCoolDog.getAge());
		myCoolDog.bark(3);
		myCoolDog.run();
		
		Dog dog2 = new Dog();
		dog2.setName("Rex");
		
		Dog dog3 = new Dog();
		Dog dog4 = new Dog();
		Dog dog5 = new Dog();
		
		System.out.println("PRINTING THE TOTAL DOG COUNT: " + Dog.getCount());
		
		dog2.printFellowCount();
		Dog.printDogCount();
		Dog.printDogAge(myCoolDog);
		
		Scanner scanner = new Scanner(System.in);
		
		Person person1 = new Person("38104242729");
		System.out.println(person1.getBirthYear());
		System.out.println(person1.getBirthMonth());
		System.out.println(person1.getBirthDayOfMonth());
		System.out.println(person1.getGender());
		if (person1.getGender() == Gender.MALE) {
			System.out.println("The person is male");
		} else if (person1.getGender() == Gender.FEMALE) {
			System.out.println("The person is female");
		} else {
			System.out.println("The person is unknown.");
		}
		
		FileLogger mySingeltong = FileLogger.getFileLoggerSingeltonInstance();
		FileLogger mySingeltong2 = FileLogger.getFileLoggerSingeltonInstance();
		
	}
	
}
