package day07.oo;

import java.io.IOException;

public class OoCryptoApp {

	public static void main(String[] args) throws IOException {

		// Võta sisendtekst.
		// Loe sisse sõnastiks (failist).
		// Teosta konverteerimine.

		System.out.println("Algne tekst:");
		System.out.println(args[0]);

		Cryptor encryptor = new Encryptor("C:/tmp/20180417/alfabeet.txt");
		String encryptedText = encryptor.translate(args[0]);

		System.out.println("Krüpteeritud tekst:");
		System.out.println(encryptedText);
		
		Cryptor decryptor = new Decryptor("C:/tmp/20180417/alfabeet.txt");
		System.out.println("Dekrüpteeritud tekst:");
		String decryptedText = decryptor.translate(encryptedText);
		System.out.println(decryptedText);		
	}

}
