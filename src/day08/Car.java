package day08;

import java.util.Objects;

public abstract class Car implements Movable, Comparable<Car> {

	private String color;
	private int maxSpeed;
	private int seatCount;
	
	@Override
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String getColor() {
		return this.color;
	}

	@Override
	public void setMaxSpeed(int speed) {
		this.maxSpeed = speed;
	}

	@Override
	public int getMaxSpeed() {
		return this.maxSpeed;
	}

	@Override
	public void setSeatCount(int seatCount) {
		this.seatCount = seatCount;
	}

	@Override
	public int getSeatCount() {
		return this.seatCount;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		Car otherCar = (Car)obj;
		boolean colorSame = this.getColor().equals(otherCar.getColor());
		boolean speedSame = this.getMaxSpeed() == otherCar.getMaxSpeed();
		boolean seatCountSame = this.getSeatCount() == otherCar.getSeatCount();
		
		return colorSame && speedSame && seatCountSame;
	}
	
	@Override
	public int hashCode() {
		int colorHashCode = Objects.hashCode(this.getColor());
		int maxSpeedHashCode = this.getMaxSpeed();
		int seatCountHashCode = this.getSeatCount();		
		return Math.abs(567 * colorHashCode * maxSpeedHashCode * seatCountHashCode);
	}
	
	@Override
	public int compareTo(Car o) {
		return this.getSeatCount() - o.getSeatCount();
	}

}







