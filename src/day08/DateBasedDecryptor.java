package day08;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DateBasedDecryptor extends DateBasedEncryptor {

	public DateBasedDecryptor(String filePath) throws IOException {
		super(filePath);
	}

	@Override
	protected Map<String, String> generateDictionary(List<String> fileLines) {
		Map<String, String> dict = new HashMap<>();
		
		int helperNum = this.generateHelperNumber();
		
		for(String charStr : fileLines) {
			char c = charStr.charAt(0);
			int charValue = (int)c;
			String encodedValue = String.valueOf(charValue + helperNum);
			dict.put(encodedValue, charStr);
		}
		
		return dict;
	}

	@Override
	public String translate(String text) {
		String decodedText = "";
		String[] encodedChars = text.split(";");
		for(String encodedChar : encodedChars) {
			decodedText = decodedText + this.dictionary.get(encodedChar);
		}
		return decodedText;
	}

}
