package day08;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import day07.oo.Cryptor;

public class DateBasedEncryptor extends Cryptor {

	public DateBasedEncryptor(String filePath) throws IOException {
		super(filePath);
	}

	@Override
	protected Map<String, String> generateDictionary(List<String> fileLines) {
		
		Map<String, String> dict = new HashMap<>();
		
		int helperNum = generateHelperNumber();
		
		for(String charStr : fileLines) {
			char c = charStr.charAt(0);
			int charValue = (int)c;
			String encodedValue = String.valueOf(charValue + helperNum) + ";";
			dict.put(charStr, encodedValue);
		}
		
		return dict;
	}

	@Override
	public String translate(String text) {
		String result = "";
		for(char c : text.toCharArray()) {
			result = result + this.dictionary.get(String.valueOf(c).toUpperCase());
		}
		return result;
	}

	protected int generateHelperNumber() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DATE);
		int helperNum = year + month + day;
		return helperNum;
	}	
}
