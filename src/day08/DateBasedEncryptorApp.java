package day08;

import java.io.IOException;

import day07.oo.Cryptor;

public class DateBasedEncryptorApp {

	public static void main(String[] args) throws IOException {
		
		DateBasedEncryptor encryptor = new DateBasedEncryptor("C:/tmp/20180418/alfabeet2.txt");
		String encodedText = encryptor.translate("Tere tere!");
		System.out.println(encodedText);
		
		Cryptor decryptor = new DateBasedDecryptor("C:/tmp/20180418/alfabeet2.txt");
		System.out.println(decryptor.translate(encodedText));
		
	}

}
