package day08;

import day06.Dog;

public class OopExcercises1 {

	public static void main(String[] args) {
		
		SmallEnglishDog smallEnglishDog = new SmallEnglishDog();
		Dog polishDog = new PolishDog();
		
		smallEnglishDog.englishStyleBarking();
		
		System.out.print("Small English dog: ");
		smallEnglishDog.bark(5);
		
		System.out.print("Polish dog: ");
		polishDog.bark(5);
		
		Dog[] barkingDogs = new Dog[2];
		barkingDogs[0] = smallEnglishDog;
		barkingDogs[1] = polishDog;
		
		makeDogsBark(barkingDogs);
		
	}
	
	public static void makeDogsBark(Dog[] dogs) {
		for(Dog barkingDog : dogs) {
			barkingDog.bark(5);
		}
	}

}
