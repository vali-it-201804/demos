package day08;

public class OopExercises2 {

	public static void main(String[] args) {
		
		Movable ferrari = new FerrariF355();
		Movable audi = new AudiA6();
		Movable renault = new RenaultMegane();
		
		ferrari.drive();
		audi.drive();
		renault.drive();		
	}

}
