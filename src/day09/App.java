package day09;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import day08.AudiA6;
import day08.Car;

public class App {

	public static void main(String[] args) {
		
		Car audiA6o1 = new AudiA6();
		audiA6o1.setColor("blue");
		audiA6o1.setMaxSpeed(180);
		audiA6o1.setSeatCount(5);
		
		Car audiA6o2 = new AudiA6();
		audiA6o2.setColor("blue");
		audiA6o2.setMaxSpeed(180);
		audiA6o2.setSeatCount(5);
		
		System.out.println("Audi A6 1 hashcode: " + audiA6o1.hashCode());
		System.out.println("Audi A6 2 hashcode: " + audiA6o2.hashCode());
		
//		audiA6o2 = audiA6o1;
//		audiA6o2.setColor("red");
//		System.out.println(audiA6o1.getColor());
		
		System.out.println("Võrdleme kahte objektipointerit: " + (audiA6o1 == audiA6o2));
		
		System.out.println("Objekti võrdlemine iseendaga: " + audiA6o1.equals(audiA6o1)); // objekt on alati võrdne iseendaga
		System.out.println("Objekti võrdlemine mittemillegagi: " + audiA6o1.equals(null)); // objekt pole kunagi võrdne mitte millegagi
		System.out.println("Auto objekti võrdlemine string objektiga: " + audiA6o1.equals("tere")); // Audo A6 ei ole võrdne stringiga.
		
		System.out.println("Objekti võrdlemine teise loogiliselt samaväärse objektiga: " + audiA6o1.equals(audiA6o2));
		
		Car car1 = new AudiA6();
		car1.setColor("blue");
		car1.setMaxSpeed(180);
		car1.setSeatCount(2);
		
		Car car2 = new AudiA6();
		car2.setColor("green");
		car2.setMaxSpeed(160);
		car2.setSeatCount(5);
		
		Car car3 = new AudiA6();
		car3.setColor("red");
		car3.setMaxSpeed(130);
		car3.setSeatCount(7);
		
		Car car4 = new AudiA6();
		car4.setColor("black");
		car4.setMaxSpeed(80);
		car4.setSeatCount(8);
		
		System.out.println(car1.compareTo(car2));
		System.out.println(car2.compareTo(car1));
		System.out.println(car2.compareTo(car2));
		
		if (car1.compareTo(car2) < 1) {
			// esimene auto on "väiksem", kui teine
		}

		if (car1.compareTo(car2) > 1) {
			// esimene auto on "suurem", kui teine
		}
		
		if (car1.compareTo(car2) == 0) {
			// esimene auto on teisega "võrdne"
		}
		
//		List<Car> cars = new ArrayList<Car>();
//		cars.add(car1);
//		cars.add(car2);
//		cars.add(car3);	
//		cars.add(car4);
//		
//		System.out.println(cars);
//		Collections.sort(cars);
//		System.out.println(cars);
	}

}
