package day09.bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Account {

	private String firstName;
	private String lastName;
	private String accountNumber;
	private int balance;

	private static Map<String, Account> clients = new HashMap<>();

	public Account(String firstName, String lastName, String accountNumber, int balance) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public static Map<String, Account> getClients() {
		return clients;
	}

	public static void setClients(Map<String, Account> clients) {
		Account.clients = clients;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return String.format("[%s, %s, %s, %s]", this.getFirstName(), this.getLastName(), this.getAccountNumber(),
				this.getBalance());
	}

	public static boolean importClients(String filePath) {
		try {
			Path path = Paths.get(filePath);
			List<String> fileLines;
			fileLines = Files.readAllLines(path);
			for (String fileLine : fileLines) {
				String[] lineParts = fileLine.split(", ");
				clients.put(lineParts[2],
						new Account(lineParts[0], lineParts[1], lineParts[2], Integer.parseInt(lineParts[3])));
			}
			return true;
		} catch (IOException e) {
			System.out.println("Error. Could not find the specified file.");
			return false;
		}
	}

	public static void displayClientInfo(String accountNumber) {
		if (clients.containsKey(accountNumber)) {
			Account account = clients.get(accountNumber);
			printAccountDetails(account);
		} else {
			System.out.println("ERROR: Cannot find account!");
		}

	}

	public static void displayClientInfo(String firstName, String lastName) {
		for (Account account : clients.values()) {
			if (account.getFirstName().equalsIgnoreCase(firstName)
					&& account.getLastName().equalsIgnoreCase(lastName)) {
				printAccountDetails(account);
				return;
			}
		}
		System.out.println("ERROR: Cannot find account!");
	}

	protected static void printAccountDetails(Account account) {
		System.out.println("------------------------------");
		System.out.println("First name: " + account.getFirstName());
		System.out.println("Last name: " + account.getLastName());
		System.out.println("Account number: " + account.getAccountNumber());
		System.out.println("Account balance: " + account.getBalance());
		System.out.println("------------------------------");
	}

	public static void transfer(String fromAccount, String toAccount, int sum) {
		if (!clients.containsKey(fromAccount)) {
			System.out.println("ERROR: Cannot find payer account!");
			return;
		}
		if (!clients.containsKey(toAccount)) {
			System.out.println("ERROR: Cannot find beneficiary account!");
			return;
		}

		Account payerAccount = clients.get(fromAccount);
		Account beneficiaryAccount = clients.get(toAccount);

		if (payerAccount.getBalance() < sum) {
			System.out.println("ERROR: The payer does not have enough money for the transfer!");
			return;
		}

		payerAccount.setBalance(payerAccount.getBalance() - sum);
		beneficiaryAccount.setBalance(beneficiaryAccount.getBalance() + sum);
		System.out.println("Transfer completed!");
	}

}
