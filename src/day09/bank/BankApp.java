package day09.bank;

import java.util.Scanner;

public class BankApp {

	public static void main(String[] args) {

		System.out.println("Bank system starts up...");
		if(!Account.importClients(args[0])) {
			return;
		}
		
		System.out.println("The bank has the following customers:");
		System.out.println(Account.getClients());
		
		Scanner inputScanner = new Scanner(System.in);
		while (true) {
			System.out.println("Please insert command:");
			// Kuula sisendit...
			String input = inputScanner.nextLine();
			// Teosta operatsioon...
			if (input.startsWith("TRANSFER")) {
				System.out.println("Starting money transfer...");
				String[] inputParts = input.split(" ");
				Account.transfer(inputParts[1], inputParts[2], Integer.parseInt(inputParts[3]));
			} else if (input.startsWith("BALANCE")) {
				System.out.println("Diplaying account balance...");
				String[] inputParts = input.split(" ");
				if (inputParts.length == 2) {
					System.out.println("Searching by account number...");
					Account.displayClientInfo(inputParts[1]);
				} else {
					System.out.println("Searching by first name and last name...");
					Account.displayClientInfo(inputParts[1], inputParts[2]);
				}
			} else {
				System.out.println("Incorrect command.");
			}
		}

	}

}
