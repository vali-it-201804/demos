package day14;

public class App {

	public static void main(String[] args) {
		
		System.out.println(getNumericSumOfChars("a"));
		System.out.println(getNumericSumOfChars("aa"));
		System.out.println(getNumericSumOfChars("aaa"));
		System.out.println(getNumericSumOfChars("b"));
		System.out.println(getNumericSumOfChars("ab"));
		
		CoffeMachine myCoffeMachine = new CoffeMachine();
		myCoffeMachine.setBeanCount(1000);
		myCoffeMachine.setWaterAmount(1000);
		System.out.println("Valmistame kohvi. Kas õnnestus? " + myCoffeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas õnnestus? " + myCoffeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas õnnestus? " + myCoffeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas õnnestus? " + myCoffeMachine.makeCoffee());
	}

	/**
	 * Ülesanne 1: Kirjutada funktsioon, mis arvutab sisendparameetrina etteantud
	 * stringi kõigi tähtede numbriliste väärtuste summa ja tagastab selle
	 * täisarvuna.
	 * 
	 * @param text
	 * @return sum
	 */
	private static int getNumericSumOfChars(String text) {
		int result = 0;

		char[] textChars = text.toCharArray();
		for (char c : textChars) {
			result += (int) c;
		}

		return result;
	}

}
