package day14;

public class CoffeMachine {

	private static final int BEAN_COUNT_FOR_CUP = 100;
	private static final int WATER_AMOUNT_FOR_CUP = 300;

	private int beanCount;
	private int waterAmount;

	public int getBeanCount() {
		return beanCount;
	}

	public void setBeanCount(int beanCount) {
		this.beanCount = beanCount;
	}

	public int getWaterAmount() {
		return waterAmount;
	}

	public void setWaterAmount(int waterAmount) {
		this.waterAmount = waterAmount;
	}

	public boolean makeCoffee() {
		if (this.getBeanCount() >= BEAN_COUNT_FOR_CUP && this.getWaterAmount() >= WATER_AMOUNT_FOR_CUP) {
			this.setBeanCount(this.getBeanCount() - BEAN_COUNT_FOR_CUP);
			this.setWaterAmount(this.getWaterAmount() - WATER_AMOUNT_FOR_CUP);
			return true;
		}
		return false;
	}
}
