package day17;

public class Exercises {

	public static void main(String[] args) {
		float priceWithoutVat1 = getProductPriceWithoutVat(120.00f);
//		System.out.println(Math.round(99.888d * 100.0d) / 100.0f);
//		System.out.println(priceWithoutVat1);
//		DecimalFormat f = new DecimalFormat("##.00");
		
//		BigDecimal bd = new BigDecimal(priceWithoutVat1);
//		bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		
//		System.out.println(bd.toString());
//		System.out.println(f.format(priceWithoutVat1));
//		
//		float convertedValue = Float.valueOf(f.format(priceWithoutVat1));
//		
//		System.out.println(convertedValue);
		
//		System.out.println(getProductPriceWithoutVat(456.99f));
//		
//		System.out.println(isPalindrome("Aias sadas saia"));
	}
	
	public static float getProductPriceWithoutVat(float priceWithVat) {
		return priceWithVat / 1.2f;
	}

	public static boolean isPalindrome(String text) {
//		String text2 = new StringBuilder(text).reverse().toString();
		String text2 = "";
		for(char c : text.toCharArray()) {
			text2 = c + text2;
		}
		return text.equalsIgnoreCase(text2);
	}
	
}
