package day17;

public class Exercises2 {

	public static void main(String[] args) {
		
		// int[][] castle1Rooms = {
		// {3, 5},
		// {8, 9},
		// {3, 1},
		// {6, 87},
		// {60, 60},
		// };
		// int[][] castle2Rooms = {
		// {3, 5},
		// {8, 6},
		// {3, 1},
		// {23, 87},
		// {64, 60},
		// };
		//
		// int castle1Surface = calculateBuildingSurface(castle1Rooms);
		// int castle2Surface = calculateBuildingSurface(castle2Rooms);

		int[] myNumbers = { 3, 7, 2, 9, 1, 4, 6, 8 };
		int myMaxNumber = getMaxNumber(myNumbers);
		System.out.println(myMaxNumber);
	}

	public static int calculateBuildingSurface(int[][] dimensionsOfBuildingRooms) {
		int totalSurface = 0;
		for (int[] oneRoomDimensions : dimensionsOfBuildingRooms) {
			totalSurface = totalSurface + calculateRoomSurface(oneRoomDimensions[0], oneRoomDimensions[1]);
		}
		return totalSurface;
	}

	public static int calculateRoomSurface(int length, int width) {
		return length * width;
	}

	public static int getMaxNumber(int[] numbers) {
		int maxNumber = 0;

		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] > maxNumber) {
				maxNumber = numbers[i];
			}
		}

		return maxNumber;
	}

}
