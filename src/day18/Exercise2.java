package day18;

import java.util.ArrayList;
import java.util.List;

public class Exercise2 {

	public static void main(String[] args) {
		
		String[] minuMassiiv = new String[] {"Laev", "Lennuk", "Rakett"};
		
		String[] minuMassiiv2 = new String[3]; // Massiivi pikkust ei saa muuta.
		minuMassiiv2[0] = "Laev";
		minuMassiiv2[1] = "Lennuk";
		minuMassiiv2[2] = "Rakett";
		
		String[] minuMassiiv3 = {"Laev", "Lennuk", "Rakett"};
		
		minuMassiiv3[2] = "Laev"; // omistamine, muutmine
		
		System.out.println(minuMassiiv3[2]);
		
//		if (minuMassiiv3[i].equals("Laev")) {
//			
//		}
		
		for (int i = 0; i < minuMassiiv3.length; i++) {
			minuMassiiv3[i] = minuMassiiv2[i];
		}
		
		// String[] result1 = multiplyText("Tere", 150);
		//
		// for (String text : result1) {
		// System.out.println(text);
		// }
		//
		// String[] result2 = multiplyText("Head aega", 6);
		//
		// for (String text : result2) {
		// System.out.println(text);
		// }

		// int[] generatedArray = generateArray(5);

//		int[] someNumbers = { 5, 7, 8, 9, 3, 5, 6, 998, 345, 456, 345 };
//		System.out.println(someNumbers[6]);
//		System.out.println(someNumbers[9]);
//		boolean isFound = isInArray(someNumbers, 9);
//		System.out.println(isFound);
		
		int[] someNumbers2 = { 5, 7, 8, 9, 3, 5, 6, 998, 345, 456, 345 };
		System.out.println(someNumbers2[3]);
		someNumbers2[3] = 123456; // omistamine
		System.out.println(someNumbers2[3]); // pärimine
		System.out.println(someNumbers2[3]);
		someNumbers2[3] = 555;
		System.out.println(someNumbers2[3]);
	}

	// Võtab sisse täisarvude massiivi, tagastab täisarvude masiivi.
	// koorutab iga sisendmasiivi elemendi kahega.
	private static int[] multiplyElementsBy2(int[] inputArray) {
		int[] outputArray = new int[inputArray.length];
		
		for (int i = 0; i < inputArray.length; i++) {
			outputArray[i] = 2 * inputArray[i];
		}
		
		return outputArray;
	}
	
	private static boolean isInArray(int[] someNumbers, int testNumber) {
		for (int index = 0; index < someNumbers.length; index++) {
			int xxx = someNumbers[index]; // Massiivi elemendi poole pöördumine.
			if (xxx == testNumber) {
				return true;
			}
		}
		return false;
	}

	public static String[] multiplyText(String text, int arrayLength) {
		String[] result = new String[arrayLength];

		for (int i = 0; i < arrayLength; i++) {
			result[i] = text;
		}

		return result;
	}

	public static int[] generateArray(int number) {
		int[] resultingArray = new int[number];
		for (int arrayIndex = 0; arrayIndex < resultingArray.length; arrayIndex++) {
			resultingArray[arrayIndex] = arrayIndex;
		}
		return resultingArray;
	}
	
}
