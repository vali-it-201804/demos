package day18;

public class Exercises1 {

	public static void main(String[] args) {
		
		float bmi = calculateBodyMassIndex(75.0f, 180);
		System.out.println(bmi);
		System.out.println(getBodyMassIndexCategory(bmi));
	}

	public static float calculateBodyMassIndex(float weightInKg, int heightInCm) {
		float heightInM = heightInCm / 100.0f;
		return Math.round(weightInKg / (Math.pow(heightInM, 2)) * 100.0f) / 100.0f;
	}
	
	public static String getBodyMassIndexCategory(float bodyMassIndex) {
		if (bodyMassIndex < 16) {
			return "Tervisele ohtlik alakaal";
		} else if (bodyMassIndex < 19) {
			return "Alakaal";
		} else if (bodyMassIndex < 25) {
			return "Normaalkaal";
		} else if (bodyMassIndex < 30) {
			return "Ülekaal";
		} else if (bodyMassIndex < 35) {
			return "rasvumine";
		} else if (bodyMassIndex < 40) {
			return "Tugev rasvumine";
		}		
		return "Tervisele ohtlik rasvumine";
	}
}
