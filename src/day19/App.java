package day19;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class App {

	public static void main(String[] args) {

		// List<String> numberOddityStatus = composeOddityList(new int[]{ 5, 7, 3, 2});
		// System.out.println(numberOddityStatus);
		//
		// List<String> inputList = new ArrayList<>();
		// inputList.add("1");
		// inputList.add("323");
		// inputList.add("998");
		// inputList.add("999");
		// List<Integer> xxx = convertFromSlToIl(inputList);
		// System.out.println(xxx);
		//
		//// Map<String, String> myMap = getPersonalData();
		//// System.out.println(myMap.get("Elukoht"));
		//
		//// System.out.println(getPersonalData().get("Elukoht"));
		// System.out.println(getPersonalData());

		int[] myArray1 = { 4, 7, 2 };
		int[] myArray2 = { 6, 2, 9 };
		int[] myResultArray = sumUpArrays(myArray1, myArray2);
		for (int sumNumber : myResultArray) {
			System.out.println(sumNumber);
		}
	}

	// Ülesanne: Kirjutada Java funktsioon, mis võtab sisendparameetrina,
	// täisarvude massiivi ja mis tagastab täisarvude massiivi.
	// Tagastatava massiivi iga element on vastava sisendmassiivi ruut.
	// {2, 7, 9} --> {4, 49, 81}.

	// Kirjutada funktsioon, mis võtab sisendparameetriks täisarvude massiivi.
	// Funktsioon tagastab tekstide listi, kus iga listielement omab väärtust kas
	// "paaris" või "paaritu" -
	// vastaalt sellele, kas antud sisendmassiivi vastav element oli paaris või
	// paaritu arv.
	//
	// sisend: { 5, 7, 3, 2} --> LIST ("paaritu", "paaritu", "paaritu", "paaris")

	public static List<String> composeOddityList(int[] numbers) {
		List<String> myResultingList = new ArrayList<>();

		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] % 2 == 0) {
				myResultingList.add("paaris");
			} else {
				myResultingList.add("paaritu");
			}
		}

		return myResultingList;
	}

	// sisendparameeter täisarv
	// Väljund täisarvude list
	// Näide1: sisend 4 --> list(0, 1, 2, 3)
	// Näide2: sisend 3 --> list(0, 1, 2)

	public static List<Integer> myMethod(int number) {
		List<Integer> result = new ArrayList<>();
		return result;
	}

	public static List<Integer> convertFromSlToIl(List<String> input) {
		List<Integer> output = new ArrayList<>();

		// for (String numberInInputList : input) {
		// output.add(Integer.parseInt(numberInInputList));
		// }

		for (int u = 0; u < input.size(); u++) {
			output.add(Integer.parseInt(input.get(u)));
		}

		return output;
	}

	// Kirjutada funktsioon, mis ei võta sisendarameetreid.
	// Mis väljastab Map-elemendi isikuandmetega.
	// Nimi --> Marek
	// Vanus --> 37
	// Elukoht --> Eesti
	// printige elukoht välja main()-meetodis.

	public static Map<String, String> getPersonalData() {
		Map<String, String> personalDataMap = new HashMap<>();

		personalDataMap.put("Nimi", "Marek");
		personalDataMap.put("Vanus", "37");
		personalDataMap.put("Elukoht", "Eesti");

		return personalDataMap;
	}

	// Teha funktsioon, mis võtab sisendiks kaks täisarvude massiivi
	// ja mis tagastab täisarvude massiivi.
	// Tagastatava massiivi iga element on kahe sisendmassiivi vastava elemendi
	// summa.
	// {1, 4, 2} ja {3, 5, 8} --> {4, 9, 10}
	// Eeldame, et sisendmassiivid on ühe pikkusega.

	public static int[] sumUpArrays(int[] arr1, int[] arr2) {
		int[] result = new int[arr1.length];
		
		for (int i = 0; i < arr1.length; i++) {
			result[i] = arr1[i] + arr2[i];
		}

		return result;
	}

}
