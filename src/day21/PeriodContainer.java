package day21;

public class PeriodContainer {

	private int periodInSeconds = 0;
	private int years = 0;
	private int months = 0;
	private int days = 0;
	private int hours = 0;
	private int minutes = 0;
	private int seconds = 0;

	public int getPeriodInSeconds() {
		return periodInSeconds;
	}

	public int getYears() {
		return years;
	}

	public int getMonths() {
		return months;
	}

	public int getDays() {
		return days;
	}

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public PeriodContainer(int periodInSeconds) {
		this.periodInSeconds = periodInSeconds;

		int yearInSeconds = 60 * 60 * 24 * 30 * 12;
		int monthInSeconds = 60 * 60 * 24 * 30;
		int dayInSeconds = 60 * 60 * 24;
		int hourInSeconds = 60 * 60;
		int minuteInSeconds = 60;

		this.years = periodInSeconds / yearInSeconds;
		this.months = (periodInSeconds - this.years * yearInSeconds) / monthInSeconds;
		this.days = (periodInSeconds - this.years * yearInSeconds - this.months * monthInSeconds) / dayInSeconds;
		this.hours = (periodInSeconds - this.years * yearInSeconds - this.months * monthInSeconds
				- this.days * dayInSeconds) / hourInSeconds;
		this.minutes = (periodInSeconds - this.years * yearInSeconds - this.months * monthInSeconds
				- this.days * dayInSeconds - this.hours * hourInSeconds) / minuteInSeconds;
		this.seconds = periodInSeconds - this.years * yearInSeconds - this.months * monthInSeconds
				- this.days * dayInSeconds - this.hours * hourInSeconds - this.minutes * minuteInSeconds;
	}

	@Override
	public String toString() {
		return String.format("Period: %s years,\n%s months,\n%s days,\n%s hours,\n%s minutes,\n%s seconds", this.years,
				this.months, this.days, this.hours, this.minutes, this.seconds);
	}

}
