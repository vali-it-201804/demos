package day24;

public class ConverterApp {

	public static void main(String[] args) {

		UsdEurCurrencyConverter usdConverter = new UsdEurCurrencyConverter();
		double valueInEur = 100.0;
		double valueInUsd = usdConverter.fromEuros(valueInEur);
		double valueInEur2 = usdConverter.toEuros(valueInUsd);

		GbpEurCurrencyConverter gbpConverter = new GbpEurCurrencyConverter();
		double valueInGbp = gbpConverter.fromEuros(valueInEur);
		double valueInEur3 = gbpConverter.toEuros(valueInGbp);
		
		System.out.println("Value in EUR: " + valueInEur);
		System.out.println("Value in USD: " + valueInUsd);
		System.out.println("Value in EUR (2): " + valueInEur2);
		System.out.println("Value in GBP: " + valueInGbp);
		System.out.println("Value in EUR (3): " + valueInEur3);
	}

}
