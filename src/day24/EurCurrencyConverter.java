package day24;

public abstract class EurCurrencyConverter {

	public double toEuros(double currency) {
		return currency / this.getExchangeRate();
	}
	
	public double fromEuros(double euros) {
		return euros * this.getExchangeRate();
	}
	
	public abstract double getExchangeRate();
}
