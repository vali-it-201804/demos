package day24;

public class GbpEurCurrencyConverter extends EurCurrencyConverter {

	@Override
	public double getExchangeRate() {
		return 0.8781d;
	}

}
