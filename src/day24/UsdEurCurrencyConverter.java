package day24;

public class UsdEurCurrencyConverter extends EurCurrencyConverter {

	@Override
	public double getExchangeRate() {
		return 1.1878d;
	}
}
