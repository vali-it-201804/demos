package day25;

public class App {

	public static void main(String[] args) {
		
//		String result = replaceWhitespaceWithNewline("Traktor	künnab põllul.");
//		System.out.print(result);
		
		String result2 = tripleCharsInText("Traktor künnab põllul.");
		System.out.println(result2);
		
	}

	private static String tripleCharsInText(String text) {
		String result = "";
		
		for (char c : text.toCharArray()) {
//			result += "" + (char)c + (char)c + (char)c;
			for (int i = 0; i < 3; i++) {
				result += c;
			}
		}
		
		return result;
	}

	private static String replaceWhitespaceWithNewline(String text) {
		
//		String result = "";
		
//		for(char c : text.toCharArray()) {
//			if (c == ' ') {
//				result += "\r\n";
//			} else {
//				result += c;
//			}
//		}		
		
		return text.replaceAll("\\s", "\r\n");
		
//		return result;
	}

}
