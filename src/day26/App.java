package day26;

public class App {

	public static void main(String[] args) {

		String text = "Veepursked annavad suhteliselt lihtsa võimaluse neid spekulatsioone kontrollida, sest tulevased kosmoseaparaadid saaksid neist piisapilvedest suhteliselt hõlpsasti proove võtta, millest elu märke otsida.";
		String censoredText = censorText(text);
		System.out.println(text);
		System.out.println(censoredText);
		
	}

	private static String censorText(String text) {
		return text.replaceAll("[^\\.\\,\\! ]", "#");
	}
}
