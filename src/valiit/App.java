package valiit;

import java.math.BigDecimal;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class App {

	public static void main(String[] args) {
		// System.out.println("Hello " + args[0] + "!");

		// byte b1 = 2;
		// byte b2 = 3;
		// short b3 = (byte) (b1 + b2);
		// System.out.println(b3);
		//
		// float f1 = 1.1f;
		// float f2 = 2.2f;
		// double f3 = f1 + f2;
		// System.out.println(f3);
		//
		// BigDecimal bd1 = new
		// BigDecimal("2323452345.0000000000000000000000000000000000000000000001");
		// BigDecimal bd2 = new BigDecimal(2);
		// BigDecimal bd3 = bd1.multiply(bd2);
		// System.out.println(bd3);
		//
		// boolean bo1 = false;
		// boolean bo2 = true;
		// boolean bo3 = bo1 && bo2;
		// System.out.println(bo1);
		// System.out.println(bo2);
		// System.out.println(bo3);
		//
		// char ch1 = 'a';
		// char ch2 = 'b';
		// char ch3 = '4';
		// System.out.println((short)ch1);
		// System.out.println((short)ch2);
		// System.out.println((short)ch3);
		// System.out.println(Integer.toBinaryString(15));

		// Integer i = 5;
		// Integer i2 = new Integer(5);
		// int j = 5;
		// Integer i3 = i + i2;
		// System.out.println(i3);
		//
		// int a = 4;
		// boolean test = (a == 4);
		// System.out.println(test);
		// String s1 = "test1";
		// String s2 = "2";
		// String s3 = s1 + s2; // test12
		// String s4 = s2 + s2; // 22
		// System.out.println(s4);
		//
		// int i1 = 2;
		// int int3 = i1 + i1; // 4;
		// System.out.println(int3);

		// int i = 1;
		// int i2 = i++;
		// System.out.println(i);
		// System.out.println(i2);
		//
		// int j1 = 10;
		// int j2 = --j1;
		// System.out.println(j1);
		// System.out.println(j2);

		// i = i + 1; // 2
		// i++; // 3
		// i--; // 2
		// ++i;
		// --i;

//		int u = 5;
//
//		u = u + 8;
//		u += 8;
//
//		u = u - 3;
//		u -= 3;

//		int i5 = 8;
//		System.out.println(i5);
////		i5 = 3 * i5;
//		i5 *= 3;
//		System.out.println(i5);
//		double d5 = i5 / 13d;
//		System.out.println(d5);
	
//		boolean b1 = true;
//		boolean b2 = false;
//		boolean b3 = !b2;
//		boolean b4 = (b1 && b2) || b3;
//		System.out.println(b4);
//		
//		if ((b1 || (((b2)))) && b3) {
//			// tee midagi
//		} else {
//			// tee midagi muud
//		}
		
		int i = 10;
		int u = 9 % 3;
		System.out.println(u);
		
	}

}
